var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BookModel = new Schema({
titulo: {type: String},
autor: {type: String},
nro_paginas: {type: Number}
});

module.exports = mongoose.model('Book', BookModel);
